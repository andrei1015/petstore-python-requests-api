import unittest
import requests
import json

class DeletePet(unittest.TestCase):
    def test_delete(self):
        url = 'https://petstore.swagger.io/v2/pet/1015'
        r = requests.delete(url)

        assert r.status_code is 200 #this fails if run before FirstTest.py
        assert r.text == ''


if __name__ == '__main__':
    unittest.main()
