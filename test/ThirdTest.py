import unittest
import requests

class login(unittest.TestCase):
    def test_create_user(self):
        url = 'https://petstore.swagger.io/v2/user'
        data = {
            "id": 0,
            "username": "andrei1015",
            "firstName": "andrei",
            "lastName": "s.",
            "email": "testemail@email.com",
            "password": "test123",
            "phone": "1234567890",
            "userStatus": 1
        }

        r = requests.post(url, json=data)


        assert r.status_code is 200

    def test_login(self):
        url = 'https://petstore.swagger.io/v2/user/login'
        param = {'username': 'andrei1015', 'password': 'test123'}
        r = requests.get(url, params=param)

        assert 'logged in user session:' in r.text

if __name__ == '__main__':
    unittest.main()
