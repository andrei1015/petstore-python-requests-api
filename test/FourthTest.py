import unittest
import requests
import json


class getByStatus(unittest.TestCase):
    def test_get_by_status_pending(self):
        url = 'https://petstore.swagger.io/v2/pet/findByStatus'
        param = {'status': 'pending'}

        r = requests.get(url, params=param)
        json_data = json.loads(r.text)
        i = 0
        for i in range(len(json_data)):
            assert json_data[i]['status'] == 'pending'

    def test_get_by_status_available(self):
        url = 'https://petstore.swagger.io/v2/pet/findByStatus'
        param = {'status': 'available'}

        r = requests.get(url, params=param)
        json_data = json.loads(r.text)
        i = 0
        for i in range(len(json_data)):
            assert json_data[i]['status'] == 'available'


    def test_get_by_status_sold(self):
        url = 'https://petstore.swagger.io/v2/pet/findByStatus'
        param = {'status': 'sold'}

        r = requests.get(url, params=param)
        json_data = json.loads(r.text)
        i = 0
        for i in range(len(json_data)):
            assert json_data[i]['status'] == 'sold'


if __name__ == '__main__':
    unittest.main()
