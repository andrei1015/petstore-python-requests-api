import requests
import unittest
import json

class UpdatePet(unittest.TestCase):
    def test_post(self):
        url = 'https://petstore.swagger.io/v2/pet/'
        data = {
          "id": 1015,
          "category": {
            "id": 1,
            "name": "dogs"
          },
          "name": "Atomic Pupper",
          "photoUrls": [
            "https://i.imgur.com/A8eQsll.jpg"
          ],
          "tags": [
            {
              "id": 1,
              "name": "doggo"
            }
          ],
          "status": "pending"
        }

        r = requests.put(url, json=data)

        assert r.status_code is 200
        assert r.json() == data

if __name__ == "__main__":
        unittest.main()