import unittest
import requests
import json


class GetData(unittest.TestCase):
    def test_parseJson(self):
        url = 'https://petstore.swagger.io/v2/pet/1015'
        r = requests.get(url)
        json_data = json.loads(r.text)

        assert json_data['tags'][0]['name'] == 'doggo'
        assert json_data['category']['name'] == 'dogs'



if __name__ == '__main__':
    unittest.main()
