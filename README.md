# QA Automation Demo

Made with Python using the [requests](http://docs.python-requests.org/en/master/) library

## Description

###### FirstTest.py

The script makes a PUT request toward the endpoint with data to update and checks the status of the response to be 
200 and the response body to be the same as the data meaning the request has been successful.

###### SecondTest.py

The script performs a GET request on the specified endpoint, parses the response body into JSON format and checks two
 sets of key: values to be as expected.

###### ThirdTest.py

The script first creates a new user then checks the user login to be working with the previously inputted data. The 
user creation request is a POST and the login is a GET. The POST requires data which is sent by the script in JSON 
format, and the GET requires URL parameters.

###### FourthTest.py

The script includes three tests, one for each officially supported statuses. Each test makes a GET towards the 
endpoint URL also including a parameter for the status. It then checks each returned item for the returned status to 
be the same as the currently searched for one.

###### FifthTest.py

The script deletes our initially created item in a DELETE request with the item id as a parameter in the URL endpoint
. It then checks for the response code to be 200 Success, and the response body to be empty.